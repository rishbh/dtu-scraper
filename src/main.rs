use scraper::{Html, Selector};
use rusqlite::{params, Connection};

const TABLES: [&str; 9] = [
	"INVALID",
	"NOTICES",
	"JOBS",
	"TENDERS",
	"LATEST_NEWS",
	"FORTHCOMING_EVENTS",
	"INVALID",
	"INVALID",
	"FIRST_YEAR_NOTICES"
];

fn fetch_tabs(
	fragment: &Html,
	conn: &Connection,
	tabno: usize
) -> Option<()> {
	let main_create_stmt = format!("
		CREATE TABLE {0} (
			id INTEGER NOT NULL PRIMARY KEY,
			title TEXT NOTNOT NULL,
			date TEXT,
			url TEXT
		);
	", TABLES[tabno]);
	let child_create_stmt = format!("
		CREATE TABLE {0}_BUTTONS (
			id INTEGER NOT NULL,
			title TEXT NOT NULL,
			url TEXT,
			FOREIGN KEY (id) REFERENCES {0} (id)
		);
	", TABLES[tabno]);
	let main_insert_stmt = format!("
			INSERT INTO {0} (id, title, date, url) VALUES (?1, ?2, ?3, ?4);
	", TABLES[tabno]);
	let child_insert_stmt = format!("
			INSERT INTO {0}_BUTTONS (id, title, url) VALUES (?1, ?2, ?3);
	", TABLES[tabno]);

	conn.execute(&main_create_stmt, params![]).unwrap();
	conn.execute(&child_create_stmt, params![]).unwrap();

	let tab = format!("#tab{}", tabno);
	let sel = Selector::parse(&tab).unwrap();
	let fragment = fragment.select(&sel).next().unwrap();

	let li_sel = Selector::parse("li").unwrap();
	let title_sel = Selector::parse("h6 a").unwrap();
	let date_sel = Selector::parse("i").unwrap();

	let mut id: usize = 0;
	for li in fragment.select(&li_sel) {
		let mut a_iter = li.select(&title_sel);

		let title = match a_iter.next() {
			Some(t) => t,
			None => continue
		};
		let title_text = title
			.text()
			.collect::<Vec<&str>>()
			.join("");

		let date_text = match li.select(&date_sel).next() {
			Some(date) => {
				let mut date_texts = date.text();
				date_texts.next().unwrap()
			},
			None => ""
		};

		let url = match title.value().attr("href") {
			Some(u) => u,
			None => ""
		};

		for a in a_iter {
			let title_text = a
				.text()
				.collect::<Vec<&str>>()
				.join("");

			let url = a.value().attr("href").unwrap();
			conn.execute(&child_insert_stmt, params![id, title_text, url]).unwrap();
		}

		conn.execute(
			&main_insert_stmt,
			params![id, title_text, date_text, url]
		).unwrap();

		id += 1;
	}

	Some(())
}

fn fetch_important_updates(fragment: &Html, conn: &Connection) -> Option<()> {
	conn.execute("
		CREATE TABLE IMPORTANT_UPDATES (
			id INTEGER NOT NULL PRIMARY KEY,
			title TEXT NOT NULL,
			date TEXT,
			url TEXT
		);
	", params![]).unwrap();
	conn.execute("
		CREATE TABLE IMPORTANT_UPDATES_BUTTONS (
			id INTEGER NOT NULL,
			title TEXT NOT NULL,
			url TEXT,
			FOREIGN KEY (id) REFERENCES IMPORTANT_UPDATES (id)
		);
	", params![]).unwrap();

	let mut id: usize = 0;
	let mut br_count: usize = 0;

	let mut main_title: &str = "";
	let mut main_url: &str = "";
	let mut date: &str = "";
	let mut has_buttons: bool = false;

	let date_sel = Selector::parse("i").unwrap();
	let item_sel = Selector::parse("marquee > *").unwrap();

	let mut items = fragment.select(&item_sel);
	loop {
		let item = items.next()?;
		match item.value().name() {
			"a" => {
				let title = item.text().next().unwrap();
				let url = if let Some(u) = item.value().attr("href") {u} else {""};

				match br_count {
					0 => {
						main_title = title;
						main_url = url;
					},

					1 => {
						conn.execute(
							"INSERT INTO IMPORTANT_UPDATES_BUTTONS (id, title, url)
							VALUES (?1, ?2, ?3);",
							params![id, title, url]
						).unwrap();
						has_buttons = true;
					},

					_ => ()
				}
			},

			"br" => {
				br_count += 1;
				if br_count == 2 && !has_buttons || br_count == 3 && has_buttons {
					conn.execute(
						"INSERT INTO IMPORTANT_UPDATES (id, title, date, url)
						VALUES (?1, ?2, ?3, ?4);"
						, params![id, main_title, date, main_url]
					).unwrap();

					id += 1;
					br_count = 0;

					main_title = "";
					main_url = "";
					date = "";
					has_buttons = false;
				}
			},

			"small" => date = item
				.select(&date_sel)
				.next()
				.unwrap()
				.text()
				.next()
				.unwrap(),

			_ => ()
		}
	}
}

fn scrape_dtu(url: &str) {
	// Load webpage
	let resp = reqwest::blocking::get(url).unwrap();
	if !resp.status().is_success() {
		panic!("Failed to load webpage!");
	}

	// Get HTML
	let body = resp.text().unwrap();
	let fragment = Html::parse_document(&body);

	// Store data from tabs into SQLite database
	let conn = Connection::open("dtu.db").unwrap();

	fetch_tabs(&fragment, &conn, 1);
	fetch_tabs(&fragment, &conn, 2);
	fetch_tabs(&fragment, &conn, 3);
	fetch_tabs(&fragment, &conn, 4);
	fetch_tabs(&fragment, &conn, 5);
	fetch_tabs(&fragment, &conn, 8);

	fetch_important_updates(&fragment, &conn);

	conn.close().unwrap();
}

fn main() {
	scrape_dtu("http://14.139.251.99");
}
